-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 03, 2014 at 07:28 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `safe_zones`
--

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE IF NOT EXISTS `devices` (
  `DID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Device ID',
  `name` varchar(50) NOT NULL COMMENT 'Name of the device',
  `is_mobile` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Is it a mobile device?',
  `UID` int(11) NOT NULL COMMENT 'UID of the device owner',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'is the device active?',
  PRIMARY KEY (`DID`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Users Device Table.' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `devices`
--

INSERT INTO `devices` (`DID`, `name`, `is_mobile`, `UID`, `status`) VALUES
(1, '015d15b4fa5c2206', 1, 1, 0),
(2, '4790f0406d40bfae', 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `FID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'File ID',
  `name` varchar(100) NOT NULL COMMENT 'File Name',
  `created_by` int(11) NOT NULL COMMENT 'UID that created the file',
  `last_accessed_by` int(11) NOT NULL COMMENT 'UID that last accessed the file',
  `location` text NOT NULL COMMENT 'The Location of the File',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Is the file currently leased?',
  `key` text NOT NULL,
  PRIMARY KEY (`FID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Table containing the files' AUTO_INCREMENT=15 ;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`FID`, `name`, `created_by`, `last_accessed_by`, `location`, `status`, `key`) VALUES
(13, 'Interim Report', 1, 1, '/dist/ops/uploads/10112251.pdf', 0, '10112251.pdf'),
(14, 'Gannt Chart', 1, 1, '/dist/ops/uploads/FYP.gan', 0, 'FYP.gan');

-- --------------------------------------------------------

--
-- Table structure for table `File_SafeZones`
--

CREATE TABLE IF NOT EXISTS `File_SafeZones` (
  `SID` int(11) NOT NULL COMMENT 'Safe Zone Id',
  `FID` int(11) NOT NULL COMMENT 'File ID'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table makes the relationship between files and Safe Zones easier to find.';

--
-- Dumping data for table `File_SafeZones`
--

INSERT INTO `File_SafeZones` (`SID`, `FID`) VALUES
(1, 9),
(1, 12),
(1, 13),
(2, 14);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `severity` int(11) NOT NULL COMMENT 'Severity Of the Log',
  `catagory` varchar(30) NOT NULL COMMENT 'Category of the Log',
  `subject` varchar(100) NOT NULL COMMENT 'Subject Of the Log',
  `full_text` mediumtext NOT NULL COMMENT 'Full Text containing details',
  `UID` int(11) NOT NULL COMMENT 'User causing the log',
  `DID` int(11) NOT NULL COMMENT 'Device causing the log',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table containing the logs pertaining to the system';

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`severity`, `catagory`, `subject`, `full_text`, `UID`, `DID`, `time`) VALUES
(0, 'user activity', 'logout', 'The user sonicwind logged out at 1392725886', 1, 0, '2014-02-18 12:18:06'),
(0, 'user activity', 'logout', 'The user sonicwind2 logged out at 1392820698', 2, 0, '2014-02-19 14:38:18'),
(0, 'user activity', 'logout', 'The user sonicwind logged out at 1392840194', 1, 0, '2014-02-19 20:03:14'),
(0, 'user activity', 'logout', 'The user sonicwind logged out at 1392840206', 1, 0, '2014-02-19 20:03:26'),
(0, 'user activity', 'logout', 'The user sonicwind logged out at 1392840629', 1, 0, '2014-02-19 20:10:29'),
(0, 'user activity', 'logout', 'The user sonicwind logged out at 1393245817', 1, 0, '2014-02-24 12:43:37'),
(0, 'user activity', 'logout', 'The user sonicwind logged out at 1393247684', 1, 0, '2014-02-24 13:14:44'),
(0, 'user activity', 'logout', 'The user sonicwind logged out at 1393247696', 1, 0, '2014-02-24 13:14:56'),
(0, 'user activity', 'logout', 'The user sonicwind logged out at 1393276190', 1, 0, '2014-02-24 21:09:50'),
(0, 'user activity', 'logout', 'The user sonicwind logged out at 1393852958', 1, 0, '2014-03-03 13:22:38');

-- --------------------------------------------------------

--
-- Table structure for table `safe_zones`
--

CREATE TABLE IF NOT EXISTS `safe_zones` (
  `SID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Safe Zone ID',
  `name` varchar(40) NOT NULL COMMENT 'Name of the Safe Zone',
  `latitude` float NOT NULL COMMENT 'Latitude of the center',
  `longitude` float NOT NULL COMMENT 'Longitude of the center',
  `radius` int(11) NOT NULL COMMENT 'The Radius of the Safe Zone',
  `buffer` int(11) NOT NULL COMMENT 'The Buffer(allows for device inaccuracies)',
  PRIMARY KEY (`SID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Table containing the Safe Zones details' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `safe_zones`
--

INSERT INTO `safe_zones` (`SID`, `name`, `latitude`, `longitude`, `radius`, `buffer`) VALUES
(1, 'Limerick City', 52.674, -8.57089, 30, 12),
(2, 'Portloise', 53.0312, -7.29487, 12, 3);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `UID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'User ID',
  `username` varchar(30) NOT NULL COMMENT 'Username ',
  `password` varchar(100) NOT NULL COMMENT 'Password',
  `outOfSystem_trust` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Is the user allowed access files outside the system?',
  `admin` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Is the user an admin?',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Is the user Online?',
  PRIMARY KEY (`UID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Table to contain all the users' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`UID`, `username`, `password`, `outOfSystem_trust`, `admin`, `status`) VALUES
(1, 'sonicwind', '24b35fefec4cf8c46d1b20b1e60f0984f760b744', 1, 1, 1),
(2, 'sonicwind2', '24b35fefec4cf8c46d1b20b1e60f0984f760b744', 0, 1, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
