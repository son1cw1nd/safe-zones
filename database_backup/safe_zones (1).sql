-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 28, 2014 at 07:29 PM
-- Server version: 5.6.14
-- PHP Version: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `safe_zones`
--

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE IF NOT EXISTS `devices` (
  `DID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Device ID',
  `name` varchar(50) NOT NULL COMMENT 'Name of the device',
  `is_mobile` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Is it a mobile device?',
  `UID` int(11) NOT NULL COMMENT 'UID of the device owner',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'is the device active?',
  PRIMARY KEY (`DID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Users Device Table.' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `FID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'File ID',
  `name` varchar(100) NOT NULL COMMENT 'File Name',
  `created_by` int(11) NOT NULL COMMENT 'UID that created the file',
  `last_accessed_by` int(11) NOT NULL COMMENT 'UID that last accessed the file',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Is the file currently leased?',
  PRIMARY KEY (`FID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table containing the files' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `File_SafeZones`
--

CREATE TABLE IF NOT EXISTS `File_SafeZones` (
  `SID` int(11) NOT NULL COMMENT 'Safe Zone Id',
  `FID` int(11) NOT NULL COMMENT 'File ID'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table makes the relationship between files and Safe Zones easier to find.';

-- --------------------------------------------------------

--
-- Table structure for table `location_logs`
--

CREATE TABLE IF NOT EXISTS `location_logs` (
  `LLID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Location Log ID',
  `UID` int(11) NOT NULL COMMENT 'User ID',
  `DID` int(11) NOT NULL COMMENT 'Device ID',
  `latitude` float NOT NULL COMMENT 'Latitude of the Device',
  `longitude` float NOT NULL COMMENT 'Longitude of the Device',
  PRIMARY KEY (`LLID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Log table to contain the locations of devices' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `severity` int(11) NOT NULL COMMENT 'Severity Of the Log',
  `catagory` varchar(30) NOT NULL COMMENT 'Category of the Log',
  `subject` varchar(100) NOT NULL COMMENT 'Subject Of the Log',
  `full_text` mediumtext NOT NULL COMMENT 'Full Text containing details',
  `UID` int(11) NOT NULL COMMENT 'User causing the log',
  `DID` int(11) NOT NULL COMMENT 'Device causing the log'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table containing the logs pertaining to the system';

-- --------------------------------------------------------

--
-- Table structure for table `safe_zones`
--

CREATE TABLE IF NOT EXISTS `safe_zones` (
  `SID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Safe Zone ID',
  `name` varchar(40) NOT NULL COMMENT 'Name of the Safe Zone',
  `latitude` float NOT NULL COMMENT 'Latitude of the center',
  `longitude` float NOT NULL COMMENT 'Longitude of the center',
  `radius` int(11) NOT NULL COMMENT 'The Radius of the Safe Zone',
  `buffer` int(11) NOT NULL COMMENT 'The Buffer(allows for device inaccuracies)',
  PRIMARY KEY (`SID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table containing the Safe Zones details' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `UID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'User ID',
  `username` varchar(30) NOT NULL COMMENT 'Username ',
  `password` varchar(100) NOT NULL COMMENT 'Password',
  `outOfSystem_trust` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Is the user allowed access files outside the system?',
  `admin` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Is the user an admin?',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Is the user Online?',
  PRIMARY KEY (`UID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Table to contain all the users' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`UID`, `username`, `password`, `outOfSystem_trust`, `admin`, `status`) VALUES
(1, 'sonicwind', 'bb5459bf19132d4dc1340654c17331df', 1, 1, 1),
(2, 'emma', 'da0d7a282884a80a1b3b0819b4f4529d', 1, 1, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
