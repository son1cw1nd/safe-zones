<?php
//Start the session for this page.
session_start();

//Include the script for this page.
include "database_conn.php";
include "cred_ops.php";
include "cryptography.php";

//Check the credentials for use on this page.
checkCredentials();

//Check Admin Status before proceeding
if ($_SESSION['admin_status'] == 1) {

    //Flag to ensure all the nessasary variables are set.
    $set = true;
    
    //Get Clean Values from the form
    if (isset($_POST['name'])) { //REQUIRED
        $name = mysql_real_escape_string($_POST['name']);
    } else {
        $set = false;
    }

    //If the file has errors or not all variables are set..
    if ($_FILES["file"]["error"] > 0 || $set = false) {
        //The user is told something is missing.
        header('Location: ../file_add.php?id=4');
    } else {
        //Path to move the encrypted file to
        $target_path = "uploads/" . basename($_FILES['file']['name']);

        //If the file moves sucessfully...
        if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {

            //Encrypt the File
            $crypt = new Cryptography();
            $crypt->Encrypt("uploads/" . basename($_FILES['file']['name']), "uploads/" . basename($_FILES['file']['name']), basename($_FILES['file']['name']));

            //Put encryption key into Database along with name of the file
            $result = mysql_query("INSERT INTO `files`(`name`, `created_by`, `last_accessed_by` , `location`,`status`, `key`) VALUES (\"" . $name . "\",\"" . $_SESSION['sess_user_id'] . "\",\"" . $_SESSION['sess_user_id'] . "\",\"" . "/dist/ops/uploads/" . basename($_FILES['file']['name']) . "\",\"" . 0 . "\",\"" . basename($_FILES['file']['name'])."\")") or trigger_error(mysql_error());

            //Get the new FID(File ID)
            $fid_result = mysql_query("SELECT `FID` from `files` WHERE `name` =\"" . $name . "\"") or trigger_error(mysql_error());
            $fid = mysql_result($fid_result, 0, "FID");

            //Add all the selected options to the SID-FID table for easy finding.
            foreach ($_POST['select'] as $selectedOption) {
                $result_insert = mysql_query("INSERT INTO `File_SafeZones`(`SID`, `FID`) VALUES (\"" . $selectedOption . "\",\"" . $fid . "\")") or trigger_error(mysql_error());
            }
    
            if ($result) {
                //Everything went well.
                header('Location: ../file_add.php?id=1');
            } else {
                //The database insertion failed.
                header('Location: ../file_add.php?id=2');
            }
        } else {
            //Something went wrong with the file situation.
            header('Location: ../file_add.php?id=3');
        }
    }
}
?>



