<?php
//Include nessassary script
include "database_conn.php";

//Returns a count of all the files on the system
function countFiles() {
    $result = mysql_query("SELECT COUNT(`FID`) FROM  `files`") or trigger_error(mysql_error());
    return mysql_result($result, 0, 0);
}

//Returns a count of all the active users
function countActiveUsers() {
    $result = mysql_query("SELECT COUNT(`UID`) FROM  `users` WHERE `status` =  '1' ") or trigger_error(mysql_error());
    return mysql_result($result, 0, 0);
}

//Returns a count of all the inactive users
function countInactiveUsers() {
    $result = mysql_query("SELECT COUNT(`UID`) FROM  `users` WHERE `status` =  '0' ") or trigger_error(mysql_error());
    return mysql_result($result, 0, 0);
}

//Returns a count of all the Safe Zones
function countSafeZones() {
    $result = mysql_query("SELECT COUNT(`SID`) FROM  `safe_zones`") or trigger_error(mysql_error());
    return mysql_result($result, 0, 0);
}

//Returns a count of all the logs on the system
function countLogs() {
    $result = mysql_query("SELECT COUNT(`UID`) FROM  `logs`") or trigger_error(mysql_error());
    return mysql_result($result, 0, 0);
}
?>
