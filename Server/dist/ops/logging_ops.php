<?php
//Include nessassary script
include "common_ops.php";

//Inputs the login attempt to the log table
function logLogin($UID) {
    $severity = 0;
    $catagory = "user activity";
    $subject = "login";
    $full_text = "The user " . getUsername($UID) . " logged in at " . getTime();
    $DID = 0;
    logEvent($severity, $catagory, $subject, $full_text, $UID, $DID);
}

//Inputs the logout attempt to the log table
function logLogout($UID) {
    $severity = 0;
    $catagory = "user activity";
    $subject = "logout";
    $full_text = "The user " . getUsername($UID) . " logged out at " . getTime();
    $DID = 0;
    logEvent($severity, $catagory, $subject, $full_text, $UID, $DID);
}

//Inputs the mobile login attempt to the log table
function logLogin_mobile($username, $uuid) {
    $severity = 0;
    $catagory = "user activity";
    $subject = "mobile login";
    $full_text = "The user " . $username . " logged in at " . getTime();
    logEvent($severity, $catagory, $subject, $full_text, getUID($username), getDID($uuid));
}

//Inputs the mobile logout attempt to the log table
function logLogout_mobile($username, $uuid) {
    $severity = 0;
    $catagory = "user activity";
    $subject = "mobile logout";
    $full_text = "The user " . $username . " logged out at " . getTime();
    logEvent($severity, $catagory, $subject, $full_text, getUID($username), getDID($uuid));
}

//Inputs the file request to the log table
function logFileRequest($username, $filename) {
    $severity = 1;
    $catagory = "file activity";
    $subject = "file access";
    $full_text = "The user " . $username . " requested the file named " . $filename . " at " . getTime();
    logEvent($severity, $catagory, $subject, $full_text, getUID($username), 0);
}

//A function that inserts the given set of variables into the database
//This cuts down on the amount of mysql queries in the logging script.
function logEvent($severity, $catagory, $subject, $full_text, $UID, $DID) {
    mysql_query("INSERT INTO `logs`(`severity`, `catagory`, `subject`, `full_text`, `UID`, `DID`) VALUES (\"" . $severity . "\",\"" . $catagory . "\",\"" . $subject . "\",\"" . $full_text . "\",\"" . $UID . "\",\"" . $DID . "\")") or trigger_error(mysql_error());
}

?>
