<?php

//Start the session
ob_start();
session_start();

//Scripts nessasary for this page
include "database_conn.php";
include "logging_ops.php";

//Check the username variable is available
if (isset($_POST["uname"])) {
    $username = mysql_real_escape_string($_POST['uname']);
}else{
    header('Location: ../index.php?id=1');
}

//Check if the password is available
if (isset($_POST["password"])) {
//Hash the given password
$password = sha1($_POST['password']);
}else{
    header('Location: ../index.php?id=1');
}

//Set up the login query
$query = "SELECT `UID`, `admin`,`password` FROM `users` WHERE `username` = '" . $username . "'";

//Execute the query
$result = mysql_query($query) or trigger_error(mysql_error());
$num = mysql_numrows($result);//Number of rows returns


if ($num == 0) { // User not found. So, redirect to login form again.
    header('Location: ../index.php?id=1');
} else {
    if ($password != mysql_result($result, 0, "password")) { // Incorrect password. So, redirect to login_form again.
        header('Location: ../index.php?id=1');
    } else { // Redirect to home page after successful login.
       //Generate the session and set the session variables
        session_regenerate_id();
        $_SESSION['sess_user_id'] = mysql_result($result, 0, "UID");
        $_SESSION['sess_username'] = $username;
        $_SESSION['sess_pass'] = $password;
        $_SESSION['admin_status'] = mysql_result($result, 0, "admin");
        session_write_close();
        
        //Log the login attempt
        logLogin( $_SESSION['sess_user_id']);
        //Update the user status
	mysql_query("UPDATE `users` SET `status`=1 WHERE `username` = \"".$username."\"") or trigger_error(mysql_error());
        //Redirect to the dashboard
        header('Location: /dist/dashboard.php');
    }
}
?>
