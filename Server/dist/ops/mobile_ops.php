<?php

//Scripts nessasary for this script
include "database_conn.php";
//include "common_ops.php";
include "logging_ops.php";
include "cryptography.php";


//Flag for the required variables
$set = true;

//mandatory variables
if (isset($_POST['user'])) {//username of the person REQUIRED
    $username = mysql_real_escape_string($_POST['user']);
} else {
    $set = false;
}

if (isset($_POST['password'])) {//password of the person, hashed to sha1 REQUIRED
    $password = mysql_real_escape_string($_POST['password']);
} else {
    $set = false;
}

if (isset($_POST['operation'])) {//operation id 1= checkLogin() 2= get filelist() 3=getFile 4 = logout REQUIRED
    $operation = mysql_real_escape_string($_POST['operation']);
} else {
    $set = false;
}

//optional varibles
if (isset($_POST['latitude'])) {//latitude of the phone Optional
    $latitude = mysql_real_escape_string($_POST['latitude']);
}

if (isset($_POST['longitude'])) {//logitude of the phone Optional
    $longitude = mysql_real_escape_string($_POST['longitude']);
}

if (isset($_POST['uuid'])) {//uuid of the phone Optional
    $uuid = mysql_real_escape_string($_POST['uuid']);
}

if (isset($_POST['filename'])) {//file wanted Optional
    $filename = mysql_real_escape_string($_POST['filename']);
}

//protected by the set flag...
if ($set) {
    switch ($operation) {
        case 1:
            checkLogin($username, $password, $uuid);
            break;
        case 2:
            getFileList($username, $password, $latitude, $longitude);
            break;
        case 3:
            getFile($username, $password, $filename);
            break;
        case 4:
            logout($username, $password, $uuid);
            break;
    }
} else {
    echo "failed";
}


/*
 * This function checks the login details provided by the android device
 * and tells the device is clear to proceed
 */

function checkLogin($username, $password, $uuid) {

    $query = "SELECT `UID`,`password` FROM `users` WHERE `username` = '" . $username . "'";

    $result = mysql_query($query) or trigger_error(mysql_error());
    $num = mysql_numrows($result);

    if ($num == 0) { // User not found.
        echo "not_found";
        return false;
    } else {
        if ($password != mysql_result($result, 0, "password")) { // Incorrect password.
            echo "incorrect_password -- " . $password;
            return false;
        } else {
            //"Query" is used when this method is used later for verification
            //Otherwise it is used to update the various tables and logs.
            if ($uuid != "query") {
                mysql_query("UPDATE `users` SET `status`=1 WHERE `username` =\"" . $username . "\"") or trigger_error(mysql_error());
                mysql_query("INSERT IGNORE INTO `devices`(`name`, `is_mobile`, `UID`, `status`) VALUES (\"" . $uuid . "\",1,\"" . mysql_result($result, 0, "UID") . "\",1)") or trigger_error(mysql_error());
                mysql_query("UPDATE `devices` SET `status`=1 WHERE ( SELECT `UID` FROM `users` WHERE `username` = \"" . $username . "\")") or trigger_error(mysql_error());
                logLogin_mobile($username, $uuid);
                echo "true";
            }
            return true;
        }
    }
}

/*
 * This function returns a file list to the mobile device,
 * this file list is based on the devices location.
 */

function getFileList($username, $password, $latitude, $longitude) {

    if (checkLogin($username, $password, "query")) {
        $return = ""; // Value used later

        $filenames = array(); //array used to hold the file names
        //Get all safe zones who have latitude/longitudes like the devices location
        $sids = mysql_query("SELECT * FROM safe_zones") or trigger_error(mysql_error());

        //for each safe zone obtained
        for ($index = 0; $index < mysql_numrows($sids); $index++) {
            
            //check to see if the device is inside the safe zone          
            $lat1 = mysql_result($sids, $index, "latitude");
            $lon1 = mysql_result($sids, $index, "longitude");
            $R = 6378.137; // Radius of earth in KM
            $dLat = ($latitude- $lat1) * pi() / 180;
            $dLon = ($longitude - $lon1) * pi() / 180;
            $a = sin($dLat / 2) * sin($dLat / 2) + cos($lat1 * pi() / 180) * cos($latitude * pi() / 180) * sin($dLon / 2) * sin($dLon / 2);
            $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
            $d = $R * $c;
            $d = $d * 1000; // meters

            if ($d <= (mysql_result($sids, $index, "radius")+mysql_result($sids, $index, "buffer")) ) {

                //get all the files for that Safe Zone
                $fid = mysql_query("SELECT `FID` FROM `File_SafeZones` WHERE `SID` = \"" . mysql_result($sids, $index, "SID") . "\"") or trigger_error(mysql_error());

                //Add all the file names to the array
                for ($index1 = 0; $index1 < mysql_numrows($fid); $index1++) {
                    $filename = mysql_query("SELECT `name` FROM `files` WHERE `FID` = \"" . mysql_result($fid, $index1, "FID") . "\"") or trigger_error(mysql_error());
                    if (mysql_numrows($filename) > 0) {
                        //Push that name to the array
                        array_push($filenames, mysql_result($filename, 0, "name"));
                    }
                }
            }
        }
        //Parse the array to a sendable string
        for ($i = 0; $i < count($filenames); $i++) {
            $temp = ":" . $filenames[$i];
            $return.=$temp;
        }
        //send the file list in sendable format.
        echo $return;
    } else {
        //user is denied access
        echo "denied";
    }
}

/*
 * This function returns a requested file to the mobile device.
 */

function getFile($username, $password, $filename) {

    if (checkLogin($username, $password, "query")) {

        //Get User ID for the user
        $uid = getUID($username);
        //Ge the File ID of the requested file
        $fid = getFID($filename);

        //Take in File name
        $result = mysql_query("SELECT `location`, `key` FROM `files` WHERE `name`= \"" . $filename . "\"") or trigger_error(mysql_error());
        //Set Status and last accessed
        mysql_query("UPDATE `files` SET `last_accessed_by`= \"" . $uid . "\",`status`= 1 WHERE `FID` = \"" . $fid . "\"") or trigger_error(mysql_error());

        //return the location
        if (!file_exists("uploads/decrypted/" . basename(mysql_result($result, 0, "location")))) {
            //Decrypt the file
            $crypt = new Cryptography();
            $crypt->Decrypt("uploads/" . basename(mysql_result($result, 0, "location")), "uploads/decrypted/" . basename(mysql_result($result, 0, "location")), mysql_result($result, 0, "key"));
        }
        //Log the request
        logFileRequest($username, $filename);
        //Return the file location to the user
        if (file_exists("uploads/decrypted/" . basename(mysql_result($result, 0, "location")))) {
            echo "/dist/ops/uploads/decrypted/" . basename(mysql_result($result, 0, "location"));
        } else {
            /*
             * This was used due to an error on a slow network connection.
             * It gives the device an extra 10 seconds to deal with the processing.
             */
            sleep(10);
            echo "/dist/ops/uploads/decrypted/" . basename(mysql_result($result, 0, "location"));
        }
    }
}

//This function logs the user out of the system
function logout($username, $password, $uuid) {
    if (checkLogin($username, $password, $uuid)) {
        //set user status to offline
        mysql_query("UPDATE `users` SET `status`= 0 WHERE `username` = \"" . $username . "\"") or trigger_error(mysql_error());
        //set device status to offline
        mysql_query("UPDATE `devices` SET `status`=0 WHERE ( SELECT `UID` FROM `users` WHERE `username` = \"" . $username . "\")") or trigger_error(mysql_error());
        //Log the logout attempt
        logLogout_mobile($username, $uuid);
    }
}
?>

