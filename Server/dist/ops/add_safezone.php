<?php
//Start the session for this page
session_start();

//Include the nessasary scripts
include "database_conn.php";
include "cred_ops.php";

//Check the session credentials.
checkCredentials();


//Check admin status before proceeding
if ($_SESSION['admin_status'] == 1) {

    //Flag to ensure all variables are set
    $set = true;

    //Get clean details from form.
    if (isset($_GET['name'])) { //REQUIRED
        $name = mysql_real_escape_string($_GET['name']);
    } else {
        $set = false;
    }
    if (isset($_GET['lat'])) { //REQUIRED
        $lat = mysql_real_escape_string($_GET['lat']);
    } else {
        $set = false;
    }
    if (isset($_GET['long'])) {//REQUIRED
        $long = mysql_real_escape_string($_GET['long']);
    } else {
        $set = false;
    }
    if (isset($_GET['rad'])) {//REQUIRED
        $radius = mysql_real_escape_string($_GET['rad']);
    } else {
        $set = false;
    }
    if (isset($_GET['buff'])) {//REQUIRED
        $buffer = mysql_real_escape_string($_GET['buff']);
    } else {
        $set = false;
    }

    //If all the nessasary variables are set.
    if ($set) {
        //Insert the Safe Zone into the Database
         $result = mysql_query("INSERT INTO `safe_zones`(`name`, `latitude`, `longitude`, `radius`, `buffer`) VALUES (\"" . $name . "\",\"" . $lat . "\",\"" . $long . "\",\"" . $radius . "\",\"" . $buffer . "\")") or trigger_error(mysql_error());
           
        if ($result) {
            //Everything went well.
            header('Location: ../safezone_add.php?id=1');
        } else {
            //Database insertion failed.
            header('Location: ../safezone_add.php?id=2');
        }
    } else {
        //Missing variables.
        header('Location: ../safezone_add.php?id=3');
    }
} else {
    //Not an administator 
    header('Location: ../safezone_add.php?id=4');
}
?>
