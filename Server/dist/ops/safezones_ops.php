<?php

//Script nessasary to run the functions in this script
include "database_conn.php";

//Return a table showing all the safe zones in the system
function listSafeZones() {
    $result = mysql_query("SELECT `SID`, `name`, `latitude`, `longitude`, `radius`, `buffer` FROM `safe_zones`") or trigger_error(mysql_error());
    buildSafeZonesTable($result);
}

//Return a table based on the searched safe zone
function searchSafeZones($keyword) {
    $result = mysql_query("SELECT `SID`, `name`, `latitude`, `longitude`, `radius`, `buffer` FROM `safe_zones` WHERE `SID` = \"" . $keyword . "\" OR `name` = \"" . $keyword . "\"") or trigger_error(mysql_error());
    buildSafeZonesTable($result);
}

//Build a table based on a set of results from the Safe Zones Table
function buildSafeZonesTable($result) {
    $numRows = mysql_numrows($result);
    echo "<table class=\"table table-striped\">";
    echo "<thead>";
    echo "<tr>";
    echo "<th>" . "SID" . " </th>";
    echo "<th>" . "Name" . " </th>";
    echo "<th>" . "Latitude" . " </th>";
    echo "<th>" . "Longitude" . " </th>";
    echo "<th>" . "Radius" . " </th>";
    echo "<th>" . "Buffer" . " </th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    for ($i = 0; $i < $numRows; $i++) {
        echo "<tr>";
        echo "<td>" . mysql_result($result, $i, "SID") . " </td>";
        echo "<td>" . mysql_result($result, $i, "name") . " </td>";
        echo "<td>" . mysql_result($result, $i, "latitude") . " </td>";
        echo "<td>" . mysql_result($result, $i, "longitude") . " </td>";
        echo "<td>" . mysql_result($result, $i, "radius") . " </td>";
        echo "<td>" . mysql_result($result, $i, "buffer") . " </td>";
        echo "</tr>";
    }
    echo "</tbody></table>";
}

?>
