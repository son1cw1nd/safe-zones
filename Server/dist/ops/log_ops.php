<?php
//Include the nessasary scripts
include "database_conn.php";

//Returns a table containing a list of logs
function listLogs() {
    $result = mysql_query("SELECT `severity`, `catagory`, `subject`, `full_text`, `UID`, `DID`,`time` FROM `logs`") or trigger_error(mysql_error());
    buildLogTable($result);
}

//Returns a table based on the results of log search
function searchLogs($keyword) {
    $result = mysql_query("SELECT `severity`, `catagory`, `subject`, `UID`, `DID` ,`time` FROM `logs` WHERE `subject` = '" . $keyword . "' OR `full_text` = '" . $keyword . "'") or trigger_error(mysql_error());
    buildLogTable($result);
}

//Build a table based on results from the Log table
function buildLogTable($result) {
    $numRows = mysql_numrows($result);
    echo "<table class=\"table table-striped\">";
    echo "<thead>";
    echo "<tr>";
    echo "<th>" . "Severity" . " </th>";
    echo "<th>" . "Catagory" . " </th>";
    echo "<th>" . "Subject" . " </th>";
    echo "<th>" . "User ID" . " </th>";
    echo "<th>" . "Device ID" . " </th>";
    echo "<th>" . "Time" . " </th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    for ($i = 0; $i < $numRows; $i++) {
        echo "<tr>";
        echo "<td>" . mysql_result($result, $i, "severity") . " </td>";
        echo "<td>" . mysql_result($result, $i, "catagory") . " </td>";
        echo "<td>" . mysql_result($result, $i, "subject") . " </td>";
        echo "<td>" . mysql_result($result, $i, "UID") . " </td>";
        echo "<td>" . mysql_result($result, $i, "DID") . " </td>";
	echo "<td>" . mysql_result($result, $i, "time") . " </td>";
        echo "</tr>";
    }
    echo "</tbody></table>";
}
