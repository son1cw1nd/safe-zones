<?php
//Script nessasary for these operations
include "database_conn.php";

//List all the active users
function listActive() {
    $result = mysql_query("SELECT `UID`, `username`,`outOfSystem_trust`, `admin`  FROM  `users` WHERE `status` =  '1'") or trigger_error(mysql_error());
    buildUserTable($result);
}

//List all the inactive users
function listInactive() {
    $result = mysql_query("SELECT `UID`, `username`,`outOfSystem_trust`, `admin`  FROM  `users` WHERE `status` =  '0'") or trigger_error(mysql_error());
    buildUserTable($result);
}

//Return a table based on the searched user
function searchUser($keyword) {
    $result = mysql_query("SELECT `UID`, `username`, `outOfSystem_trust`, `admin` FROM `users` WHERE `UID` = \"".$keyword."\" OR `username` = \"" . $keyword . "\"") or trigger_error(mysql_error());
    buildUserTable($result);
}

//Build a table based on a set of results based on the user table.
function buildUserTable($result) {
    $numRows = mysql_numrows($result);
    echo "<table class=\"table table-striped\">";
    echo "<thead>";
    echo "<tr>";
    echo "<th>" . "UID" . " </th>";
    echo "<th>" . "Username" . " </th>";
    echo "<th>" . "Trusted Outside of the System" . " </th>";
    echo "<th>" . "Administrator" . " </th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    for ($i = 0; $i < $numRows; $i++) {
        echo "<tr>";
        echo "<td>" . mysql_result($result, $i, "UID") . " </td>";
        echo "<td>" . mysql_result($result, $i, "username") . " </td>";
        echo "<td>" . convertTinyInt(mysql_result($result, $i, "outOfSystem_trust")) . " </td>";
        echo "<td>" . convertTinyInt(mysql_result($result, $i, "admin")) . " </td>";
        echo "</tr>";
    }
    echo "</tbody></table>";
}

//Convert a tiny int to a String readable by humans.
function convertTinyInt($result) {
    if ($result == 1) {
        return "Yes";
    } else {
        return "No";
    }
}

?>
