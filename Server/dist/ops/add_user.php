<?php
//Start the session for this page.
session_start();

//Include the script for this page.
include "database_conn.php";
include "cred_ops.php";

//Check the credentials for use on this page.
checkCredentials();

//Flag to ensure all the nessasary variables are set.
$set = true;

//Get Clean Values from the form
if (isset($_GET['username'])) { //REQUIRED
    $username = mysql_real_escape_string($_GET['username']);
} else {
    $set = false;
}
if (isset($_GET['password'])) {//REQUIRED
    $password = mysql_real_escape_string($_GET['password']);
} else {
    $set = false;
}
if (isset($_GET['password2'])) {//REQUIRED
    $password2 = mysql_real_escape_string($_GET['password2']);
} else {
    $set = false;
}
if (isset($_GET['admin'])) {//MAY BE BLANK IF NOT SET
    $request_admin = mysql_real_escape_string($_GET['admin']);
    if ($request_admin == "on") {
        $request_admin = 1;
    }
} else {
    $request_admin = 0;
}

if (isset($_GET['ost'])) {//MAY BE BLANK IF NOT SET
    $request_ost = mysql_real_escape_string($_GET['ost']);
    if ($request_ost == "on") {
        $request_ost = 1;
    }
} else {
    $request_ost = 0;
}

//Check admin status and all variables being set
if (!$_SESSION['admin_status'] == 1 || $set == false) {
    //Something is not set or user is not an admin.
    header('Location: ../user_add.php?id=3');
} else {
    //Check for non-matching passwords
    if ($password != $password2) {
        header('Location: ../user_add.php?id=2');
    } else {
        //Insert the user into the database
        $result = mysql_query("INSERT INTO `users`(`username`, `password`, `outOfSystem_trust`, `admin`) VALUES (\"" . $username . "\",\"" . sha1($password) . "\",\"" . $request_ost . "\",\"" . $request_admin . "\")") or trigger_error(mysql_error());
        if ($result) {
            //Everything went well.
            header('Location: ../user_add.php?id=1');
        } else {
            //Database insertion failed.
            header('Location: ../user_add.php?id=4');
        }
    }
}
?>
