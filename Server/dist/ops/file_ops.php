<?php
//Include the nessasary scripts for this script
include "database_conn.php";
include "common_ops.php";

//Returns a table containing all files
function listFiles() {
    $result = mysql_query("SELECT  `FID` , `name`, `created_by` ,  `last_accessed_by`, `status` FROM  `files` ") or trigger_error(mysql_error());
    buildFileTable($result);
}

//Get a count of all the files in the system
function countFiles() {
    $result = mysql_query("SELECT COUNT(`FID`) FROM  `files`") or trigger_error(mysql_error());
    return mysql_result($result, 0, 0);
}

//Returns a table based on a file search
function searchFile($keyword) {
    $result = mysql_query("SELECT  `FID` , `name`, `created_by` ,  `last_accessed_by` , `status` FROM  `files`  WHERE `FID` = '" . $keyword . "' OR `name` = '" . $keyword . "'") or trigger_error(mysql_error());
    buildFileTable($result);
}

//Convert an integer status to human readable format.
function convertStatus($status) {

    if ($status == "0") {
        return "Not Leased";
    } else {
        return "Leased";
    }
}

//Populate the multiple select in the form with the available Safe Zones
function populateSelect() {
    $result_select = mysql_query("SELECT  `SID`,`name` FROM  `safe_zones`") or trigger_error(mysql_error());
    $numRows = mysql_numrows($result_select);

    for ($i = 0; $i < $numRows; $i++) {
        echo "<option value=\"" . mysql_result($result_select, $i, "SID") . "\">" . mysql_result($result_select, $i, "name") . "</option>";
    }
}

//Build a table based on the Safe Zones database table.
function buildFileTable($result) {
    $numRows = mysql_numrows($result);
    echo "<table class=\"table table-striped\">";
    echo "<thead>";
    echo "<tr>";
    echo "<th>" . "FID" . " </th>";
    echo "<th>" . "Filename" . " </th>";
    echo "<th>" . "Created By" . " </th>";
    echo "<th>" . "Last Accessed By" . " </th>";
    echo "<th>" . "Status" . " </th>";
    echo "</tr>";
    echo "</thead>";
    echo "<tbody>";
    for ($i = 0; $i < $numRows; $i++) {
        echo "<tr>";
        echo "<td>" . mysql_result($result, $i, "FID") . " </td>";
        echo "<td>" . mysql_result($result, $i, "name") . " </td>";
        echo "<td>" . getUsername(mysql_result($result, $i, "created_by")) . " </td>";
        echo "<td>" . getUsername(mysql_result($result, $i, "last_accessed_by")) . " </td>";
        echo "<td>" . convertStatus(mysql_result($result, $i, "status")) . " </td>";
        echo "</tr>";
    }
    echo "</tbody></table>";
}

?>
