<?php
//start the session for this page
session_start();
//this has a method that checks for session variables.
include "ops/cred_ops.php";
//Checks session variables and acts accordingly.
checkCredentials();

//switch value that may get used later.
$switch = 0;
//check on run if a value was sent to the page and set the $switch value to the value
if (isset($_GET["id"])) {
    $switch = $_GET["id"];
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta content="IE=edge" charset=UTF-8' http-equiv='Content-Type'/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Safe Zone Addition">
        <meta name="author" content="Paul Buckley">
        <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

        <title>Safe Zones</title>

        <!-- Bootstrap core CSS -->
        <link href="../../dist/css/bootstrap.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header"><!--This is used for the mobile site -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="dashboard.php">Dashboard</a>
                </div>
                 <!--Start Navigation Bar -->
                <div class="navbar-collapse collapse">
                     <!--Start Menu -->
                    <ul class="nav navbar-nav">
                        <li><a href="dashboard.php">Home</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown">Users <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="users.php?id=1">Active Users</a></li>
                                <li><a href="users.php?id=2">Inactive Users</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown">Files <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="files.php?id=1">Active Files</a></li>
                            </ul>
                        </li>
                        <li class="active"><a href="safezones.php?id=1">Safe Zones</a></li>
                        <li><a href="logs.php?id=1">Logs</a></li>
                        <li><a class="navbar-right" href="ops/logout_ops.php">Logout</a></li>
                    </ul><!-- End Menu -->
                    <!-- Start Form to search Safe Zones -->
                    <form class="navbar-form navbar-right" role="search" name="safezone" action="safezones.php?id=3">
                        <div class="form-group">
                            <input type="text" class="form-control" name="safezone" placeholder="Search SafeZones">
                        </div>
                    </form><!-- End Form -->
                </div><!--End Navigation Bar -->
            </div><!-- End Container -->
        </div><!-- End Navigation Bar Collapse -->

        <hr/>
        <hr/>
        
        <div class="container">
            <!-- This form passes the map the location value the user desires -->
            <form id="mapform" action="#">
                <p>
                    <input id="mapinput" type="text" style="width: 80%;" value="Limerick, Ireland" maxlength="100">
                    <input type="submit" style="width: 18%;" value="Find">
                </p>
            </form>
            <!-- Div to hold the map -->
            <div id="mapdiv" style="height: 400px;"></div>
            <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&amp;sensor=false&amp;key=AIzaSyD3VSb2IYSKdPdcDWFffqh0pGy9S47Klzk"></script>
            <script type="text/javascript">
                google.maps.event.addDomListener(window, "load", function() {
                    /*
                     * Intialise the map
                     * initial the 2 circle to be used
                     * Set Map to the location.
                     */
                    var circle = new google.maps.Circle();
                    var circle2 = new google.maps.Circle();
                    var map = new google.maps.Map(document.getElementById("mapdiv"), {
                        center: new google.maps.LatLng(52.664619660815731, -8.626517081638223),
                        zoom: 13,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    });
                    //
                    // initialize marker
                    //
                    var marker = new google.maps.Marker({
                        position: map.getCenter(),
                        draggable: true,
                        map: map
                    });
                    //
                    // intercept map and marker movements
                    // Output values to the text box
                    //
                    google.maps.event.addListener(map, "idle", function() {
                        // marker.setPosition(map.getCenter());
                        document.getElementById("lat").value = map.getCenter().lat().toFixed(15);
                        document.getElementById("long").value = map.getCenter().lng().toFixed(15);
                        var radius_cir = parseFloat(document.getElementById('rad').value);
                        var buffer_cir = radius_cir + parseFloat(document.getElementById('buff').value);
                        var radOptions = {
                            strokeColor: '#FF0000',
                            strokeOpacity: 0.8,
                            strokeWeight: 2,
                            fillColor: '#FF0000',
                            fillOpacity: 0.35,
                            map: map,
                            center: marker.getPosition(),
                            radius: radius_cir
                        };

                        var buffOptions = {
                            strokeColor: '#00FF00',
                            strokeOpacity: 0.8,
                            strokeWeight: 2,
                            map: map,
                            center: marker.getPosition(),
                            radius: buffer_cir
                        };

                        // Add the circle for this city to the map.
                        circle.setMap(null);
                        circle2.setMap(null);
                        circle.setOptions(radOptions);
                        circle2.setOptions(buffOptions);
                        circle.bindTo('center', marker, 'position');
                        circle2.bindTo('center', marker, 'position');
                    });

                    google.maps.event.addListener(marker, "dragend", function(mapEvent) {
                        map.panTo(mapEvent.latLng);
                        //
                    });
                    //
                    // initialize geocoder
                    //
                    var geocoder = new google.maps.Geocoder();
                    google.maps.event.addDomListener(document.getElementById("mapform"), "submit", function(domEvent) {
                        if (domEvent.preventDefault) {
                            domEvent.preventDefault();
                        } else {
                            domEvent.returnValue = false;
                        }
                        geocoder.geocode({
                            address: document.getElementById("mapinput").value
                        }, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                var result = results[0];
                                document.getElementById("mapinput").value = result.formatted_address;
                                if (result.geometry.viewport) {
                                    map.fitBounds(result.geometry.viewport);
                                    marker.setPosition(map.getCenter());
                                }
                                else {
                                    map.setCenter(result.geometry.location);
                                    marker.setPosition(map.getCenter());
                                }
                            } else if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
                                alert("Sorry, the geocoder failed to locate the specified address.");
                            } else {
                                alert("Sorry, the geocoder failed with an internal error.");
                            }
                        });
                    });
                });
            </script>

            <br>
             <?php
                 //This switch tells the user the status of the file addition
                 //operation.
             switch ($switch) {
                 case 1:
                echo "<div class=\"alert alert-success alert-dismissable\">";
                echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
                echo "<strong>Congratulations!</strong> Safe Zone has been added!</div>"; 
                break;
                 case 2:
                echo "<div class=\"alert alert-danger alert-dismissable\">";
                echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
                echo "<strong>Warning!</strong> Something went wrong with the database insertion...</div>"; 
                break;
                 case 3:
                echo "<div class=\"alert alert-danger alert-dismissable\">";
                echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
                echo "<strong>Warning!</strong> Missing nessasary variables.</div>"; 
                break;
                 case 4:
                echo "<div class=\"alert alert-danger alert-dismissable\">";
                echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
                echo "<strong>Warning!</strong> Not an administrator</div>"; 
                break;
                }
             ?>
            <!-- Latitude Text Box-->
            <form class="form-horizontal" action="ops/add_safezone.php" >
                <fieldset>

                    <!-- Form Name -->
                    <legend>Safe Zone Creation</legend>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="name">Name</label>  
                        <div class="col-md-4">
                            <input id="name" name="name" type="text" placeholder="Name" class="form-control input-md">
                            <span class="help-block">help</span>  
                        </div>
                    </div> 

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="lat">Lat:</label>  
                        <div class="col-md-4">
                            <input id="lat" name="lat" type="text" placeholder="Latitude" class="form-control input-md" readonly required>
                            <span class="help-block">help</span>  
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="long">Long:</label>  
                        <div class="col-md-4">
                            <input id="long" name="long" type="text" placeholder="Longitude" class="form-control input-md" readonly required>
                            <span class="help-block">help</span>  
                        </div>
                    </div>

                    <!-- Text input  -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="rad">Radius</label>  
                        <div class="col-md-4">
                            <input id="rad" name="rad" type="text" placeholder="Radius" class="form-control input-md">
                            <span class="help-block">help</span>  
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="buff">Buffer</label>  
                        <div class="col-md-4">
                            <input id="buff" name="buff" type="text" placeholder="Buffer" class="form-control input-md">
                            <span class="help-block">help</span>  
                        </div>
                    </div>

                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="sub"> </label>
                        <div class="col-md-4">
                            <button id="sub" name="sub" type="submit" class="btn btn-success">Save</button>
                        </div>
                    </div>

                </fieldset>
            </form>
            <footer>
                <p>&copy; Paul Buckley 2014</p>
            </footer>
        </div> <!-- /container -->


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="../../dist/js/bootstrap.min.js"></script>
    </body>
</html>