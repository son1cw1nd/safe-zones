<?php

	//include "ops/cred_ops.php";
	// checkCredentials();

//TODO: Change Key for google api
//TODO: Put map into a panel.
//TODO: Modify code to draw circles based on the radius & buffer
//TODO: Add form elements
//TODO: Pass to add_safezone.php
//TODO: Style Buttons To Bootstrap
?>
<!DOCTYPE html>
<html>
<head>
<meta content="IE=edge" charset=UTF-8' http-equiv='Content-Type'/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

<title>Safe Zones</title>

<!-- Bootstrap core CSS -->
    <link href="../../dist/css/bootstrap.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
<body>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="dashboard.php">Dashboard</a>
        </div>
        <div class="navbar-collapse collapse">
           <ul class="nav navbar-nav">
            <li><a href="dashboard.php">Home</a></li>
            <li class="dropdown">
 		 <a class="dropdown-toggle" data-toggle="dropdown">Users <b class="caret"></b></a>
 			 <ul class="dropdown-menu">
   			 <li><a href="users.php?id=1">Active Users</a></li>
   			 <li><a href="users.php?id=2">Inactive Users</a></li>
  		</ul>
	    </li>

            <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown">Files <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="files.php?id=1">Active Files</a></li>
                            </ul>
                        </li>
            <li class="active"><a href="safezones.php?id=1">Safe Zones</a></li>
            <li><a href="logs.php?id=1">Logs</a></li>
          </ul>
 	<form class="navbar-form navbar-right" role="search" name="safezone" action="safezones.php?id=3">
     		 <div class="form-group">
        	   <input type="text" class="form-control" name="safezone" placeholder="Search SafeZones">
      		 </div>
    	</form>
        </div><!--/.navbar-collapse -->
      </div>
    </div>


    <hr/>
        <hr/>
 <div class="container">
<form id="mapform" action="#">
  <p>
    <input id="mapinput" type="text" style="width: 80%;" value="Disneyland, 1313 S Harbor Blvd, Anaheim, CA 92802, USA" maxlength="100">
    <input type="submit" style="width: 18%;" value="Find">
  </p>
</form>
<div id="mapdiv" style="height: 400px;"></div>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&amp;sensor=false&amp;key=AIzaSyD3VSb2IYSKdPdcDWFffqh0pGy9S47Klzk"></script>
<script type="text/javascript">
  google.maps.event.addDomListener(window, "load", function() {
    //
    // initialize map
    //
    var circle = new google.maps.Circle();
    var circle2 = new google.maps.Circle();
    var map = new google.maps.Map(document.getElementById("mapdiv"), {
      center: new google.maps.LatLng(33.808678, -117.918921),
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    //
    // initialize marker
    //
    var marker = new google.maps.Marker({
      position: map.getCenter(),
      draggable: true,
      map: map
    });
    //
    // intercept map and marker movements
    // Output values to the text box
    //
    google.maps.event.addListener(map, "idle", function() {
     // marker.setPosition(map.getCenter());
      document.getElementById("lat").value = map.getCenter().lat().toFixed(15);
      document.getElementById("long").value = map.getCenter().lat().toFixed(15);
      var radius_cir =  parseFloat(document.getElementById('rad').value);
      var buffer_cir =  radius_cir + parseFloat(document.getElementById('buff').value);
      var radOptions = {
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35,
      map: map,
      center: marker.getPosition(),
      radius: radius_cir
    };
     
    var buffOptions = {
      strokeColor: '#00FF00',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      map: map,
      center: marker.getPosition(),
      radius: buffer_cir
    };
	
    // Add the circle for this city to the map.
	circle.setMap(null);
        circle2.setMap(null);
        circle.setOptions(radOptions);
        circle2.setOptions(buffOptions);
        circle.bindTo('center', marker, 'position');
	circle2.bindTo('center', marker, 'position');
    });

    google.maps.event.addListener(marker, "dragend", function(mapEvent) {
      map.panTo(mapEvent.latLng);
	//
    });
    //
    // initialize geocoder
    //
    var geocoder = new google.maps.Geocoder();
    google.maps.event.addDomListener(document.getElementById("mapform"), "submit", function(domEvent) {
      if (domEvent.preventDefault){
        domEvent.preventDefault();
      } else {
        domEvent.returnValue = false;
      }
      geocoder.geocode({
        address: document.getElementById("mapinput").value
      }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          var result = results[0];
          document.getElementById("mapinput").value = result.formatted_address;
          if (result.geometry.viewport) {
            map.fitBounds(result.geometry.viewport);
          }
          else {
            map.setCenter(result.geometry.location);
          }
        } else if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
          alert("Sorry, the geocoder failed to locate the specified address.");
        } else {
          alert("Sorry, the geocoder failed with an internal error.");
        }
      });
    });
  });
</script>

<br>
	<!-- Latitude Text Box-->

	<form class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend>Safe Zone Creation</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="lat">Lat:</label>  
  <div class="col-md-4">
  <input id="lat" name="lat" type="text" placeholder="Latitude" class="form-control input-md" readonly required>
  <span class="help-block">help</span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="long">Long:</label>  
  <div class="col-md-4">
  <input id="long" name="long" type="text" placeholder="Longitude" class="form-control input-md" readonly required>
  <span class="help-block">help</span>  
  </div>
</div>

<!-- Text input <input type="button" onclick="moveMarker()" value="Move Marker"> -->
<div class="form-group">
  <label class="col-md-4 control-label" for="rad">Radius</label>  
  <div class="col-md-4">
  <input id="rad" name="rad" type="text" placeholder="Radius" class="form-control input-md">
  <span class="help-block">help</span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="buff">Buffer</label>  
  <div class="col-md-4">
  <input id="buff" name="buff" type="text" placeholder="Buffer" class="form-control input-md">
  <span class="help-block">help</span>  
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="sub">Save</label>
  <div class="col-md-4">
    <button id="sub" name="sub" class="btn btn-primary">Button</button>
  </div>
</div>

</fieldset>
</form>




	<!-- Longitude Text Box-->
	<!-- Radius Input Box-->
	<!-- Buffer Text Box-->

      <footer>
        <p>&copy; Paul Buckley 2014</p>
      </footer>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
  </body>
</html>
