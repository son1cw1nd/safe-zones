<?php
//start the session for this page
session_start();
//this script contains the relevent functions for this page.
include "ops/file_ops.php";
//this has a method that checks for session variables.
include "ops/cred_ops.php";
//Checks session variables and acts accordingly.
checkCredentials();

//switch value that may get used later.
$switch = 0;
//check on run if a value was sent to the page and set the $switch value to the value
if (isset($_GET["id"])) {
    $switch = $_GET["id"];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">
        <title>Safe Zones</title>

        <!-- Bootstrap core CSS -->
        <link href="../../dist/css/bootstrap.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header"><!--This is used for the mobile site -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="dashboard.php">Dashboard</a>
                </div>
                <!-- Start Navigation Bar -->
                <div class="navbar-collapse collapse">
                    <!-- Start Menu -->
                    <ul class="nav navbar-nav">
                        <li><a href="dashboard.php">Home</a></li>

                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown">Users <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="users.php?id=1">Active Users</a></li>
                                <li><a href="users.php?id=2">Inactive Users</a></li>
                            </ul>
                        </li>

                        <li class="active dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown">Files <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="files.php?id=1">Active Files</a></li>
                            </ul>
                        </li>

                        <li><a href="safezones.php?id=1">Safe Zones</a></li>
                        <li><a href="logs.php?id=1">Logs</a></li>
                        <li><a class="navbar-right" href="ops/logout_ops.php">Logout</a></li>
                    </ul><!-- End Menu -->
                    <!-- Form to search files on the system -->
                    <form class="navbar-form navbar-right" role="search" name="filename" action="files.php?id=3">
                        <div class="form-group">
                            <input type="text" class="form-control" name="filename" placeholder="Search Files">
                        </div>
                    </form><!-- End Form -->
                </div><!--End Navigation Bar -->
            </div><!-- End Container -->
        </div><!-- End Navigation Bar Collapse -->


        <hr/>
        <hr/>
        
        <div class="container">
            <?php
                 //This switch tells the user the status of the file addition
                 //operation.
             switch ($switch) {
                 case 1:
                echo "<div class=\"alert alert-success alert-dismissable\">";
                echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
                echo "<strong>Congratulations!</strong> File has been added!</div>"; 
                break;
                 case 2:
                echo "<div class=\"alert alert-danger alert-dismissable\">";
                echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
                echo "<strong>Warning!</strong> Something went wrong with the database insertion...</div>"; 
                break;
                 case 3:
                     echo "<div class=\"alert alert-danger alert-dismissable\">";
                echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
                echo "<strong>Warning!</strong> Something is wrong with the file...</div>"; 
                break;
                }
             ?>
            
            <form class="form-horizontal" action="ops/add_file.php"method="post"
                  enctype="multipart/form-data">
                <fieldset>

                    <!-- Form Name -->
                    <legend>Add File</legend>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="name">File Name</label>  
                        <div class="col-md-4">
                            <input id="name" name="name" type="text" placeholder="File Name" class="form-control input-md" required="">
                            <span class="help-block">Name the uploaded file</span>  
                        </div>
                    </div>

                    <!-- File Button --> 
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="file">Choose File</label>
                        <div class="col-md-4">
                            <input id="file" name="file" class="input-file" type="file">
                        </div>
                    </div>

                    <!--Multiple Select -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="select">Select Safe Zones</label>
                        <div class="col-md-4">
                            <select id="select" name="select[]" class="form-control" multiple="multiple">
                                <?php populateSelect(); ?>
                            </select>
                        </div>
                    </div>

                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="sub">Save File</label>
                        <div class="col-md-4">
                            <button id="sub" name="sub" type="submit" class="btn btn-success">Save</button>
                        </div>
                    </div>

                </fieldset>
            </form>
            <hr>
            <footer>
                <p>&copy; Paul Buckley 2014</p>
            </footer>
        </div> <!-- /container -->


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="../../dist/js/bootstrap.min.js"></script>
    </body>
</html>