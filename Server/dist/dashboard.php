<?php
//start the session for this page
session_start();
//this script contains the relevent functions for this page.
include "ops/dashboard_ops.php";
//this has a method that checks for session variables.
include "ops/cred_ops.php";
//Checks session variables and acts accordingly.
checkCredentials();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Dashboard">
        <meta name="author" content="Paul Buckley">
        <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

        <title>Safe Zones</title>

        <!-- Bootstrap core CSS -->
        <link href="../../dist/css/bootstrap.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <!--Start Naviagation Bar -->
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header"><!--This is used for the mobile site -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="dashboard.php">Dashboard</a>
                </div>
                <div class="navbar-collapse collapse">
                    <!--Start Menu -->
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="dashboard.php">Home</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown">Users <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="users.php?id=1">Active Users</a></li>
                                <li><a href="users.php?id=2">Inactive Users</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown">Files <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="files.php?id=1">Active Files</a></li>
                            </ul>
                        </li>
                        <li><a href="safezones.php?id=1">Safe Zones</a></li>
                        <li><a href="logs.php?id=1">Logs</a></li>
                        <li><a class="navbar-right" href="ops/logout_ops.php">Logout</a></li>
                    </ul><!--End Menu -->
                </div>
            </div>
        </div> <!-- End Navigation Bar-->
        <hr/>
        <hr/>
        <div class="container">
            <!-- Start Row of columns -->
            <div class="row">
                <!-- Begin Column 1 -->
                <div class="col-sm-6">
                    <!--Start Users Panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title "><span class = "glyphicon glyphicon-user"></span> Users</h3>
                        </div>
                        <div class="panel-body">
                            Online: <?php $a = countActiveUsers();echo $a; ?><br/>
                            Offline: <?php $i = countInactiveUsers();echo $i; ?>
                            <?php
                            //This script gets the ratio of online to 
                            //offline users for the progress bar
                            $total = $a + $i;
                            $per_a = ($a * 100) / $total;
                            $per_i = 100 - $per_a;
                            ?>
                            <!-- Start Progress Bar -->
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" <?php echo "style=\"width:" . $per_a . "%\"" ?>></div>
                                <div class="progress-bar progress-bar-danger" <?php echo "style=\"width:" . $per_i . "%\"" ?>></div>
                            </div><!-- End Progress Bar -->
                            <a class="btn btn-primary" href="users.php?id=1" role="button">Manage</a>
                        </div>
                    </div><!-- End Users Panel-->
                    
                    <!-- Start Files Panel-->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class = "glyphicon glyphicon-file"></span> Files</h3>
                        </div>
                        <div class="panel-body">
                            <?php echo "Files: " . countFiles();?>
                            <br/>
                            <a class="btn btn-primary" href="files.php?id=1" role="button">Manage</a>
                        </div>
                    </div><!-- End Files Panel -->
                </div> <!-- End Column 1 -->
                
                <!-- Start Column 2 -->
                <div class="col-sm-6">
                    
                    <!-- Start Safe Zones Panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class = "glyphicon glyphicon-map-marker"></span> Safe Zones</h3>
                        </div>
                        <div class="panel-body">
                                <?php echo "SafeZones: " . countSafeZones(); ?>
                            <br/>
                            <a class="btn btn-primary" href="safezones.php?id=1" role="button">Manage</a>
                        </div>
                    </div> <!-- End Safe Zones Panel -->
                    
                    <!-- Start Logs Panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class = "glyphicon glyphicon-tasks"></span> Logs</h3>
                        </div>
                        <div class="panel-body">
                                <?php echo "Logs: " . countLogs(); ?>
                            <br/>
                            <a class="btn btn-primary" href="logs.php?id=1" role="button">View</a>
                        </div>
                    </div> <!-- End Logs Panel -->
                </div><!--End Column 2 -->
            </div><!-- End Row -->
            <hr>
            <footer>
                <p>&copy; Paul Buckley 2014</p>
            </footer>
        </div> <!-- End container -->

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="../../dist/js/bootstrap.min.js"></script>
    </body>
</html>

