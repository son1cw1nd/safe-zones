<?php
//SNIPPIT taken from: http://bootsnipp.com/snippets/featured/parallax-login-form

//start the session for this page
session_start();
//switch value that may get used later.
$switch = 0;
//check on run if a value was sent to the page.(used later to tell the user about an incorrect login).
if (isset($_GET["id"])) {
    $switch = $_GET["id"];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Login Page">
        <meta name="author" content="Paul Buckley">
        <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

        <link rel="stylesheet" type="text/css" href="css/tween.css">


        <title>Safe Zones</title>

        <!-- Bootstrap core CSS -->
        <link href="../../dist/css/bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/signin.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <div class="container">                      
            <div class="col-md-4 col-md-offset-4">
		<!-- Beginning of Sign In Panel-->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h2 class="form-signin-heading text-info">Please sign in</h2>
                        <?php
			//If the script returns 1, that tells this switch to output a message telling the user the password is wrong.
                        switch ($switch) {
                            case 1:
                                echo "<div class=\"alert alert-danger alert-dismissable\">";
                                echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
                                echo "<strong>Sorry! </strong> Username or Password are incorrect!</div>";
                                break;
                        }
                        ?>
                    </div>
                    <div class="panel-body">
			<!-- This form accepts the username and password provided by the user and sends them to the login check script --->
                        <form class="form-signin" role="form" method = "post" action="ops/login_ops.php">

                            <input type="text" class="form-control" name = "uname" id = "uname" placeholder="Username" required autofocus>
                            <input type="password" class="form-control" name = "password" id = "password" placeholder="Password" required>
                            <button class="btn btn-lg btn-primary btn-block" type="submit"">Sign in</button>
                        </form>
			<!-- End Form--->
                    </div>
                </div>
		<!-- End Panel--->
            </div>
	<!-- End Column Offset--->
        </div> 
	<!-- /container -->


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="../../dist/js/bootstrap.min.js"></script>
        <script src="http://mymaplist.com/js/vendor/TweenLite.min.js"></script>
        <script src="js/tween.js"></script>
    </body>
</html>

