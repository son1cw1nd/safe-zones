<?php
//start the session for this page
session_start();
//this script contains the relevent functions for this page.
include "ops/file_ops.php";
//this has a method that checks for session variables.
include "ops/cred_ops.php";
//Checks session variables and acts accordingly.
checkCredentials();

//Checks to see if the id field has been set and sets the switch value to it.
if (isset($_GET["id"])) {
    $switch = $_GET["id"];
}
//If the file name value is set, the user is searching by that filename,
//switch is set to 2.
if (isset($_GET["filename"])) {
    $file = $_GET["filename"];
    $switch = 2;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Files Page">
        <meta name="author" content="Paul Buckley">
        <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">
        <title>Safe Zones</title>

        <!-- Bootstrap core CSS -->
        <link href="../../dist/css/bootstrap.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header"><!--This is used for the mobile site -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="dashboard.php">Dashboard</a>
                </div>
                <!-- Start Navigation Bar -->
                <div class="navbar-collapse collapse">
                    <!-- Start Menu -->
                    <ul class="nav navbar-nav">
                        <li><a href="dashboard.php">Home</a></li>

                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown">Users <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="users.php?id=1">Active Users</a></li>
                                <li><a href="users.php?id=2">Inactive Users</a></li>
                            </ul>
                        </li>

                        <li class="active dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown">Files <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="files.php?id=1">Active Files</a></li>
                            </ul>
                        </li>

                        <li><a href="safezones.php?id=1">Safe Zones</a></li>
                        <li><a href="logs.php?id=1">Logs</a></li>
                        <li><a class="navbar-right" href="ops/logout_ops.php">Logout</a></li>
                    </ul> <!-- End Menu -->
                    <!-- Form to search the files available in the system -->
                    <form class="navbar-form navbar-right" role="search" name="filename" action="files.php?id=3">
                        <div class="form-group">
                            <input type="text" class="form-control" name="filename" placeholder="Search Files">
                        </div>
                    </form><!-- End Form -->
                </div><!--End Navigation Bar -->
            </div><!-- End container -->
        </div><!-- End Navigation Bar collapse -->


        <hr/>
        <hr/>
        
        <div class="container">
            <div class="btn-group"><!-- Button to navigate to the add file page -->
                <a href = "file_add.php"><button type="button" class="btn btn-primary btn-default">Add File</button></a>
            </div>
            <hr/>
                <!--Start Panel Containing Table -->
                <div class="panel panel-default">
                    <!-- Panel contents -->
                    <div class="panel-heading">Files</div>

                    <!-- Table -->
                    <?php
                    /*
                     * $switch values and meanings:
                     * 1 = list all files
                     * 2 = search files
                     */
                    
                    //These methods automatically compile a table with the results
                    //from a database
                    if ($switch == 1) {
                        listFiles();
                    } else {
                        searchFile($file);
                    }
                    ?>
                </div><!--End Panel -->
            <hr>
            <footer>
                <p>&copy; Paul Buckley 2014</p>
            </footer>
        </div> <!-- /container -->


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="../../dist/js/bootstrap.min.js"></script>
    </body>
</html>

