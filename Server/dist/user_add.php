<?php
//start the session for this page
session_start();
//this has a method that checks for session variables.
include "ops/cred_ops.php";
//Checks session variables and acts accordingly.
checkCredentials();
//switch value that may get used later.
$switch = 0;
//check on run if a value was sent to the page and set the $switch value to the value
if (isset($_GET["id"])) {
    $switch = $_GET["id"];
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Add User Page">
        <meta name="author" content="Paul Buckley">
        <link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">

        <title>Safe Zones</title>

        <!-- Bootstrap core CSS -->
        <link href="../../dist/css/bootstrap.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header"><!--This is used for the mobile site -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="dashboard.php">Dashboard</a>
                </div>
                <!-- Start Navigation Bar -->
                <div class="navbar-collapse collapse">
                    <!--Start Menu-->
                    <ul class="nav navbar-nav">
                        <li><a href="dashboard.php">Home</a></li>
                        <li class="active dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown">Users <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="users.php?id=1">Active Users</a></li>
                                <li><a href="users.php?id=2">Inactive Users</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown">Files <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="files.php?id=1">Active Files</a></li>
                            </ul>
                        </li>
                        <li><a href="safezones.php?id=1">Safe Zones</a></li>
                        <li><a href="logs.php?id=1">Logs</a></li>
                        <li><a class="navbar-right" href="ops/logout_ops.php">Logout</a></li>
                    </ul><!-- End Menu -->
                    <form class="navbar-form navbar-right" role="search" name="username" action="users.php?id=3">
                        <div class="form-group">
                            <input type="text" class="form-control" name="username" placeholder="Search Users">
                        </div>
                    </form><!-- Form To Search Users -->
                </div><!--/End Navigation Bar -->
            </div>
        </div><!-- End Navigation Bar Collapse -->
        <hr/>
        <hr/>

        <div class="container">
            <?php
            //This code implements the ID System to tell the user if anything went wrong.
            switch ($switch) {
                case 1:
                    echo "<div class=\"alert alert-success alert-dismissable\">";
                    echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
                    echo "<strong>Congratulations!</strong> User has been added!</div>";
                    break;
                case 2:
                    echo "<div class=\"alert alert-danger alert-dismissable\">";
                    echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
                    echo "<strong>Warning!</strong> The given passwords don't match...</div>";
                    break;
                case 3:
                    echo "<div class=\"alert alert-danger alert-dismissable\">";
                    echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
                    echo "<strong>Warning!</strong> You are not an administrator!</div>";
                    break;
                case 4:
                    echo "<div class=\"alert alert-danger alert-dismissable\">";
                    echo "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>";
                    echo "<strong>Warning!</strong> Something is wrong with the database insertion...</div>";
                    break;
            }
            ?>
            <!-- This Form takes in the details to add a user to the database -->
            <form class="" role="search" name="add_user" action="/dist/ops/add_user.php">
                <fieldset>
                    <legend>Add User Details</legend>
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
                        <label for="password">Password</label>
                        <input type="password" class = "form-control" name = "password"placeholder="Password" required>
                        <label for="password2">Re-Type Password</label>
                        <input type="password" class = "form-control" name = "password2"placeholder="Password" required>
                        <label for="admin" name = "admin_label">Grant Administrator <br> </label>
                        <input type="checkbox" name ="admin"><br><br>
                        <label for="ost" name = "ost_label">Trusted outside the system <br> </label>
                        <input type="checkbox" name ="ost"><br><br>
                        <input class="btn btn-lg btn-success" type="submit" value="Save User">
                        </fieldset>
                        </form>
                        <hr/>
                        <footer>
                            <p>&copy; Paul Buckley 2014</p>
                        </footer>
                    </div> <!--End container -->


                    <!-- Bootstrap core JavaScript
                    ================================================== -->
                    <!-- Placed at the end of the document so the pages load faster -->
                    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
                    <script src="../../dist/js/bootstrap.min.js"></script>
                    </body>
                    </html>

