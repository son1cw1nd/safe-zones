package com.paulbuckley.serverCommunications;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.util.Log;

public class SendData {

	//String used to contact the server
	public String website = "http://thecakesbar.com";
	//The location of the mobile_ops file on the server
	private String SERVER_ADDRESS = website + "/dist/ops/mobile_ops.php";

	//This method interfaces with the server so it can check the login
	//Name Value Pairs represent the post variables to the server
	public boolean checkLogin(String user, String password) {
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("user", user));
		nameValuePairs.add(new BasicNameValuePair("password", password));
		nameValuePairs.add(new BasicNameValuePair("uuid",android.os.Build.SERIAL));
		nameValuePairs.add(new BasicNameValuePair("operation", String.valueOf(1)));
		String response = sendData(nameValuePairs, SERVER_ADDRESS);
		Log.i("Response", response);
		if (response.contains("true")) {
			return true;	 
		} else {
			return false;
		}
	}

	//This method interfaces with the server so it obtain the file list
	//Name Value Pairs represent the post variables to the server
	public String getFileList(String user, String password,String lat, String lon) {
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("user", user));
		nameValuePairs.add(new BasicNameValuePair("password", password));
		nameValuePairs.add(new BasicNameValuePair("latitude", lat));
		nameValuePairs.add(new BasicNameValuePair("longitude", lon));
		nameValuePairs.add(new BasicNameValuePair("operation", String.valueOf(2)));
		Log.i("Latitude", lat);
		Log.i("Longitude", lon);
		return sendData(nameValuePairs, SERVER_ADDRESS); 
	}
	
	//This method interfaces with the server so it obtain a file
	//Name Value Pairs represent the post variables to the server
	public String getFile(String user, String password,String filename) {
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("user", user));
		nameValuePairs.add(new BasicNameValuePair("password", password));
		nameValuePairs.add(new BasicNameValuePair("filename", filename.trim()));
		nameValuePairs.add(new BasicNameValuePair("operation", String.valueOf(3)));
		return website + sendData(nameValuePairs, SERVER_ADDRESS); 
	}
	
	//This method interfaces with the server to tell the system I'm logging out
	//Name Value Pairs represent the post variables to the server
	public void logout(String user, String password) {
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("user", user));
		nameValuePairs.add(new BasicNameValuePair("password", password));
		nameValuePairs.add(new BasicNameValuePair("uuid",android.os.Build.SERIAL));
		nameValuePairs.add(new BasicNameValuePair("operation", String.valueOf(4)));
		sendData(nameValuePairs, SERVER_ADDRESS); 
	}

	/**
	 * This method sends the data to the server in a JSON fashion.
	 * 
	 * @param data
	 *            Data to be sent to the server
	 * @param URL
	 *            url to send the data to.
	 */
	public String sendData(ArrayList<NameValuePair> data, String URL) {
		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(URL);
			httppost.setEntity(new UrlEncodedFormEntity(data));
			HttpResponse response = httpclient.execute(httppost);
			String ServerResponse = EntityUtils.toString(response.getEntity());
			Log.i("DEBUG_SAFEZONES_STATUS_LINE", response.getStatusLine().toString());
			Log.i("DEBUG_SAFEZONES_RESPONSE", ServerResponse);
			return ServerResponse;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return "";
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			return "";
		} catch (IOException e) {
			e.printStackTrace();
			Log.e("Not Connected", "Not Connected");
			return "";
		}

	}
}