package com.paulbuckley.security;

import java.io.File;

/*
 * This class is to manage all the file operations on the system
 */
public class File_Ops {

	/*
	 * This method wipes all the files in a given directory,
	 * it assumes no directories inside the main directory
	 */
	public void wipeFiles(String input_directory){
		File directory = new File(input_directory);
		
		if (directory.isDirectory()) {
	        String[] files = directory.list();
	        for (int i = 0; i < files.length; i++) {
	            new File(directory, files[i]).delete();
	        }
	    }		
	}
}
