package com.paulbuckley.security;

import java.util.ArrayList;

public class FradulentLocation {
	
	//Private ArrayLists for holding the latitude and longitude details
	private ArrayList<Double> lats = new ArrayList<Double>();
	private ArrayList<Double> longs = new ArrayList<Double>();
	
	/*
	 * This method checks to see how similar location requests have been,
	 * this can be used to detect fraudulent locations being sent to the device.
	 */
	public boolean checkLocations(){
		double reference_lat = 0,reference_lon = 0;
		int percent_match = 0;
		if(lats.size() > 4)
		for(int i = 0; i < lats.size(); i++){
			  reference_lat =lats.get(i);
			  reference_lon =longs.get(i);
			  
			if(lats.contains(reference_lat) || longs.contains(reference_lon))
			{
				for (int j = 0; j < lats.size(); j++) {
					if(lats.get(j).equals(reference_lat) && longs.get(j).equals(reference_lon)){
						percent_match++;
					}
				}
			}
			if(calculatedPercentage(lats.size(), percent_match) >= 50){
				return true;
			}
		}
		return false;
		
	}

	/*
	 * Inserts the given latitude into the arraylist
	 */
	public void insertLatitude(double latitude){
		lats.add(latitude);
	}
	
	/*
	 * Inserts the given longitude into the arraylist
	 */
	public void insertLongtude(double longitude){
		longs.add(longitude);
	}
	
	/*
	 * Calculates the percentage of two given numbers,
	 * used in the checkLocations Function.
	 */
	private int calculatedPercentage(int size, int match){
		return (match/size)*100;
	}
}
