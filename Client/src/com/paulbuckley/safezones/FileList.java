package com.paulbuckley.safezones;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.paulbuckley.safezones.Login;
import com.paulbuckley.safezones.R;
import com.paulbuckley.security.File_Ops;
import com.paulbuckley.security.FradulentLocation;
import com.paulbuckley.sensors.LocationService;
import com.paulbuckley.serverCommunications.SendData;

@SuppressLint("SdCardPath")
public class FileList extends Activity {

	private String username = "";
	private String password = "";
	private String filename = "";
	private String actual_filename = "";
	private String masterFolder = "/sdcard/Safe Zones/";
	private FradulentLocation fl;

	ListView fileListView;
	LocationService locationService;

	ArrayList<String> fileList = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_file_list);

		// Get username and password from the last activity
		username = getIntent().getStringExtra("username");
		password = getIntent().getStringExtra("password");

		// Get the filelist view
		fileListView = (ListView) findViewById(R.id.listView1);
		//start fradulent location class
		fl = new FradulentLocation();

		/*
		 * Set an on click listener on the file list, this list allows the user
		 * to select a file on the list and download it.
		 */
		fileListView
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent,
							final View view, int position, long id) {
						filename = fileList.get(position);// Get the clicked
															// filename
						// Toast to tell the user a file is downloading
						Toast.makeText(getApplicationContext(), "Downloading",
								Toast.LENGTH_LONG).show();
						// Download the selected file
						new DownloadFileAsync().execute();
					}
				});

		// Start the location service on start up
		locationService = new LocationService(getApplicationContext());
		// Create a new file list task instance
		getFileListTask gft = new getFileListTask();
		// Get the file list (android does not allow network in the foreground)
		gft.execute((Void) null);
	}

	/*
	 * This method inflates the menu for use in the menu bar
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.filelist, menu);
		return true;
	}

	/*
	 * This method allows the me to specify the actions of buttons in the menu
	 * bar
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		// The refresh button re-downloads the file list
		case R.id.action_refresh:
			Toast.makeText(this, "Refreshing", Toast.LENGTH_SHORT).show();
			locationService = new LocationService(getApplicationContext());
			new getFileListTask().execute();
			break;
		// This button logs the user out
		case R.id.action_Logout:
			Toast.makeText(this, "Logging out", Toast.LENGTH_SHORT).show();
			new logoutTask().execute();
			break;
		case R.id.action_about:
			startActivity(new Intent(getApplicationContext(), About.class));
			break;
		default:
			break;
		}

		return true;
	}

	/*
	 * Base Code:
	 * http://www.vogella.com/code/com.vogella.android.listview.withanimations
	 * /src
	 * /com/vogella/android/listview/withanimation/ListViewExampleActivity.html
	 * 
	 * The code was necessary because of a bug in Android that displayed white
	 * text on a white background. Using this basic example code I was able to
	 * override the view and set the text to black.
	 */
	private class StableArrayAdapter extends ArrayAdapter<String> {

		// Hash Map manage the entries
		HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

		public StableArrayAdapter(Context context, int textViewResourceId,
				ArrayList<String> objects) {
			super(context, textViewResourceId, objects);
			for (int i = 0; i < objects.size(); ++i) {
				mIdMap.put(objects.get(i), i);
			}
		}

		@Override
		public long getItemId(int position) {
			String item = getItem(position);
			return mIdMap.get(item);
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

		/*
		 * This is where I construct the workaround for the text color.
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = super.getView(position, convertView, parent);
			// Set the text color to black.
			TextView textView = (TextView) view
					.findViewById(android.R.id.text1);
			textView.setTextColor(Color.BLACK);

			return view;
		}

	}

	/*
	 * This is a background thread that downloads the filelist in the background
	 * to display to the user. Android does not allow network on the main
	 * thread.
	 */
	private class getFileListTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {

			// Get the location
			locationService.getLocation();
			
			// This is necessary to wait for the location to be obtained
			while (locationService.canGetLocation() != true) {
			}

			fl.insertLatitude(locationService.getLatitude());
			fl.insertLongtude(locationService.getLongitude());
			if(fl.checkLocations())
			{
				Toast.makeText(getApplicationContext(), "Fradulent Location Detected", Toast.LENGTH_LONG).show();
				finish();
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
			// Instantiate the server communications class
			SendData sd = new SendData();

			// Get the result from the server
			String result = sd.getFileList(username, password,
					String.valueOf(locationService.getLatitude()),
					String.valueOf(locationService.getLongitude()));

			// Split the result to get the file names
			String[] split = result.split(":");

			// Put the array into an arrayList for the listView
			fileList = new ArrayList<String>();
			for (int i = 1; i < split.length; i++) {
				fileList.add(split[i]);
			}
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			// If the list was successfully downloaded
			if (success) {
				// Add the array adapter to the listView for the user to see
				StableArrayAdapter arrayAdapter = new StableArrayAdapter(
						getApplicationContext(),
						android.R.layout.simple_list_item_1, fileList);
				fileListView.setAdapter(arrayAdapter);
				// Stop the GPS
				locationService.stopUsingGPS();
				locationService.stopSelf();
			} else {
				// Stop the GPS
				locationService.stopUsingGPS();
				locationService.stopSelf();
			}
		}

		@Override
		protected void onCancelled() {
			// Stop the GPS
			locationService.stopUsingGPS();
			locationService.stopSelf();
		}
	}

	/*
	 * This is a background thread that downloads a given file in the
	 * background. Android does not allow network on the main thread.
	 * 
	 * Base Code: http://www.java-samples.com/showtutorial.php?tutorialid=1521
	 * 
	 * As I had no previous experience in this and kept obtaining errors,
	 * I required a sample to work from. Following the sample, I was able to
	 * heavily modify it to match my requirements.
	 */
	private class DownloadFileAsync extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... aurl) {

			// This is the file location received from the server
			String recieved_url = new SendData().getFile(username, password,
					filename).trim();

			try {

				// A file representing the master folder
				File folder = new File(masterFolder);
				// Create the master folder if it doesn't exist.
				if (!folder.exists()) {
					folder.mkdir();
				}

				// Connect to the location and download the file
				URL url = new URL(recieved_url);
				URLConnection connection = url.openConnection();
				connection.connect();

				// Get the file length
				int fileLength = connection.getContentLength();
				Log.i("File Length", fileLength+"");
				// If the file exists..
				if (fileLength != -1) {
					// Open a steam to the file
					InputStream input = new BufferedInputStream(
							url.openStream());

					// Split the URL string
					String actual_name[] = recieved_url.split("/");
					// Get the filename from the end of the url
					actual_filename = actual_name[actual_name.length - 1]
							.trim();
					// Wipe any files in the folder.
					// This is also useful for security as
					// only 1 file can exist on the device at one time
					new File_Ops().wipeFiles(masterFolder);

					// Set up a file object for the downloading file
					File f = new File(masterFolder + actual_filename);

					// Create the file on the system
					f.createNewFile();

					// Create the output stream to the file object
					OutputStream output = new FileOutputStream(f);

					// Data buffer
					byte data[] = new byte[1024];

					int count;
					long total = 0;

					while ((count = input.read(data)) != -1) {
						total += count;
						publishProgress("" + (int) ((total * 100) / fileLength));
						output.write(data, 0, count);
					}

					output.flush();// Flush the output stream
					output.close();// Close the output stream
					input.close();// Close the input stream
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;

		}

		protected void onProgressUpdate(String... progress) {
			//For Debugging purposes
			Log.i("File Download", progress[0]);
		}

		@Override
		protected void onPostExecute(String unused) {
			//This intent is to ask the system to open the file
			Intent intent = new Intent();
			//Tell the system I wish to view the file
			intent.setAction(android.content.Intent.ACTION_VIEW);
			//Get the file name and location
			File file = new File(masterFolder + actual_filename);
			//Pass the location and file type to the system
			intent.setDataAndType(Uri.fromFile(file), "text/*");
			//Run the intent.
			startActivity(intent);
		}
	}

	/*
	 * This background task logs the user out
	 * and wipes all the files on the system.
	 * It also returns the user to the login page
	 */
	private class logoutTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected Boolean doInBackground(Void... params) {
			new SendData().logout(username, password);
			new File_Ops().wipeFiles(masterFolder);
			startActivity(new Intent(getApplicationContext(), Login.class));
			return true;
		}

	}
}
