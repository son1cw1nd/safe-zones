package com.paulbuckley.safezones;

import com.paulbuckley.serverCommunications.SendData;

import android.os.Bundle;
import android.widget.TextView;
import android.app.Activity;

public class About extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		TextView server = (TextView) findViewById(R.id.server);
		server.setText(new SendData().website);
	}
}
